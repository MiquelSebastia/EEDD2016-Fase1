with Ada.integer_text_io; use Ada.integer_text_io;
with pparaula; use pparaula;
with Ada.Containers; use Ada.Containers;
with Ada.Strings.hash;
package diccionari_hs is
    type diccionari is limited private;

    space_overflow: exception;

    procedure buit    (s: out diccionari);
    procedure posa    (s: in out diccionari; x: in tparaula);
    function existeix (s: in diccionari; x: in tparaula) return boolean;
    --function Ada.Strings.Hash(Key: String) return Containers.Hash_Type;
private
    --TODO move constants to separate package
        max_id: constant integer:= 340000;
        max_ch: constant integer:= 4000000;
        -- GENRAL DEFS
            type name_id is new integer range 0..max_id;
            null_id: constant name_id:= 0;
        -- EOGD
    --END of constants

    b: constant Ada.Containers.Hash_Type:=
            Ada.Containers.Hash_Type(max_id);
    subtype hash_index is Ada.Containers.Hash_Type range 0..b-1;

    type list_item is
        record
            psh: name_id;
            ptc: natural; --Pointer to the table of characters
        end record;

    type id_table   is array(name_id) of list_item;
    type dispersion_table is array(hash_index) of name_id;
    subtype characters_table is string(1..max_ch);

--    null_id: constant name_id := 0;

--    type node is
--        record
--	   value: meus_caracters;
--	   next: pnode;
--        end record;

    type diccionari is
       record
           td: dispersion_table;
           tid: id_table;
           tc: characters_table;
           nid: name_id;
           nc: natural;
       end record;

end diccionari_hs;
