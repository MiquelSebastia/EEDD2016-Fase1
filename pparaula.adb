package body pparaula is
    
--    package Characters is new Ada.Containers.Hashed_Maps
--        (Key_type => meus_caracters,
--         Element_type => character,
--         Hash => ID
    type tcharacter_set is array (character) of boolean;

    character_set: tcharacter_set := 
        ('a'..'z'|'�'|'�'|'�'|'�'|'�'|'�'|'�'|'�'|'�'|'�' => true,
        others => false);

    type tmeu_character_set is array (character) of meus_caracters;
    meu_character_set: tmeu_character_set :=
        ('a' => a,
        'b' => b,
        'c' => c,
        'd' => d,
        'e' => e,
        'f' => f,
        'g' => g,
        'h' => h,
        'i' => i,
        'j' => j,
        'k' => k,
        'l' => l,
        'm' => m,
        'n' => n,
        'o' => o,
        'p' => p,
        'q' => q,
        'r' => r,
        's' => s,
        't' => t,
        'u' => u,
        'v' => v,
        'w' => w,
        'x' => x,
        'y' => y,
        'z' => z,
        '�' => �,
        '�' => �,
        '�' => �,
        '�' => �,
        '�' => �,
        '�' => �,
        '�' => �,
        '�' => �,
        '�' => '�',
        '�' => '�',
        others=>'$');

    function inRange(c: in character) return boolean is
    begin
        return character_set(c);
    end inRange;

    procedure put (p : in tparaula) is
    begin
        put( toString(p) );
    end put;

    procedure put(f : in out File_Type; p : in tparaula) is
    begin
        --put(f, toString(p) );
        put_line(f, toString(p) );
    end put;

    function "=" (a, b : in tparaula) return boolean is
    begin
        return toString(a) = toString(b);
    end "=";

    function llargaria(p : in tparaula) return tllargaria is
    begin
        --return toString(p)'length;
        return p.llargaria;
    end llargaria;

    function buida(p : in tparaula) return boolean is
    begin
        return toString(p)'length = 0;
    end buida;

    function caracter(p : in tparaula; posicio : in rang_lletres) return character is
    begin
        if posicio > p.llargaria then return '$'; end if;
        return p.lletres(posicio);
    end caracter;

    function meu_caracter(p: in tparaula; posicio: in rang_lletres) return meus_caracters is
    begin
        if posicio > p.llargaria then return '$'; end if;
        return meu_character_set(p.lletres(posicio));
    end meu_caracter;

    function toString(p: in tparaula) return  String is
        ll: tllargaria renames p.llargaria;
        s: String (1..ll);
    begin
        for i in 1..ll loop
            s(i) := p.lletres(i);
        end loop;
        return s;
    end toString;

    procedure copiar(desti : out tparaula; origen : in tparaula) is
        ll: tllargaria renames origen.llargaria;
    begin
        desti.llargaria := ll;
        for i in 1..ll loop
            desti.lletres(i) := origen.lletres(i);
        end loop;
    end copiar;

    procedure open(origen : out OrigenParaules) is
        f: file_type := Ada.Text_IO.Standard_Input;
    begin
        origen.defitxer := false;
--<<<<<<< HEAD
--        origen.fitxer := f;
--        --origen.fitxer := Ada.Text_IO.Standard_Input;
--        --origen.fitxer := Standard_Input;
--=======
	origen.lletra := ' ';

-->>>>>>> origin/HEAD
    end open;

    procedure open(origen : out OrigenParaules; nom : in String) is
    begin
        origen.defitxer := true;
        open(origen.fitxer, in_file, nom);
        origen.columna := 0;
        origen.linea := 0;
    end open;

    procedure close(origen :in out OrigenParaules) is
    begin
        close(origen.fitxer);
    end close;

    --procedure get(origen : in out OrigenParaules; p: out tparaula) is
    --begin
        --p.llargaria := 0;
        --get(origen.fitxer, origen.lletra);

        --while origen.lletra in 'a'..'z' loop
            --p.llargaria := p.llargaria + 1;
          --  p.lletres(p.llargaria) := origen.lletra;
        --    get(origen.fitxer, origen.lletra);
      --  end loop;
    --end get;

    procedure get(origen : in out OrigenParaules; p: out tparaula; linea: out integer; columna: out integer) is
        ch_std: character;
	lin: integer renames origen.linea;
        col: integer renames origen.columna;

    begin

    p.llargaria := 0;
    if origen.defitxer then
        get(origen.fitxer, origen.lletra);
        origen.lletra := Ada.Characters.Handling.To_Lower(origen.lletra);
        while inRange(origen.lletra) loop

            col := col + 1;
            columna := col;
            p.llargaria := p.llargaria + 1;
            p.lletres(p.llargaria) := origen.lletra;
            if end_of_line(origen.fitxer) then
                columna := col - p.llargaria; col := 0;
                lin := lin + 1; linea := lin-1;
                exit;
            end if;
            -- exit when End_Of_Line(origen.fitxer);
            get(origen.fitxer, origen.lletra);

        end loop;
        if not End_Of_Line(origen.fitxer) then
            col := col + 1; --tenim en compte els espais
            columna := columna - p.llargaria; --apuntar a l'inici de la paraula
            linea := lin;
        end if;


	else

		get(ch_std);
		while inRange(ch_std) loop
		    p.llargaria := p.llargaria + 1;
		    p.lletres(p.llargaria) := ch_std;
              exit when End_Of_Line;
		    get(ch_std);
		end loop;

	end if;

    end get;

    function eof(origen : in OrigenParaules) return boolean is
        defitxer : boolean renames origen.defitxer;
        fitxer: File_Type renames origen.fitxer;
    begin
        if defitxer then
            return End_of_file(fitxer);
        else
            return End_of_file;
        end if;
    end eof;
end pparaula;
