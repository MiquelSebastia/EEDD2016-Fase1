with pparaula; use pparaula;
package diccionari_simple is
    D_TAMANY: constant integer:= 200000;
    subtype d_llargaria is  integer range 0..D_TAMANY;

    type diccionari is private;

    type index_cell is new integer range 0..D_TAMANY;
    type cell_pool is array (index_cell range 1..index_cell'last) of tparaula;

    procedure buit(d: out diccionari);
    procedure posa(d: in out diccionari; p: in tparaula);
    function  existeix(d: in diccionari; p: in tparaula) return boolean;
private
    type diccionari is
        record
            index: index_cell;
            paraules: cell_pool;
        end record;

end diccionari_simple;
