with Ada.text_io; use Ada.text_io;
with Ada.integer_text_io; use Ada.integer_text_io;

with pparaula; use pparaula;
package diccionari_trc is
    type diccionari is private;

    space_overflow: exception;

    procedure buit  (s: out diccionari);
    procedure posa    (s: in out diccionari; x: in tparaula);
    function existeix  (s: in diccionari; x: in tparaula) return boolean;
private
    type node;
    type pnode is access node;

    type node is
        record
            value: meus_caracters;
            sibling, child: pnode;
        end record;

    type diccionari is
        record
            root: pnode:= null;
        end record;
end diccionari_trc;
