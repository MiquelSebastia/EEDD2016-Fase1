package body diccionari_simple is

   procedure buit(d: out diccionari) is
   begin
       d.index := 0;
   end buit;


   procedure posa(d: in out diccionari; p: in tparaula) is
       index: index_cell renames d.index;
   begin
       index := index + 1;
       d.paraules(index) := p;
   end posa;


   function  existeix(d: in diccionari; p: in tparaula) return boolean is
       index: index_cell renames d.index;
       trobada: boolean := false;
   begin

	for i in 1..index loop
		if d.paraules(i) = p then trobada := true; end if;
          exit when trobada;
	end loop;

	return trobada;
   end existeix;

end diccionari_simple;
