package body diccionari_trc is

    procedure buit (s: out diccionari) is
    begin
        s.root := null;
    end buit;


    procedure posa0 (pn: in out pnode; pos: in tllargaria; x: in tparaula) is
        aux: pnode := pn;
    begin
        for i in pos+1..llargaria(x)+1 loop
            aux.child := new node;
            aux.child.all := (meu_caracter(x, i), null, null);
            aux := aux.child;
        end loop;
    end posa0;

    procedure posa (s: in out diccionari; x: in tparaula) is
        aux: pnode;
        posat: boolean := false;
    begin
        if s.root = null then
            s.root := new node;
            s.root.all := (none, null, null);
        end if;
        aux := s.root;
        for i in 1 .. llargaria(x)+1 loop
            while aux.sibling /= null and aux.value /= meu_caracter(x, i) loop
                --exit when aux.sibling = null;
                aux := aux.sibling;
            end loop;
            if aux.sibling = null and aux.value /= meu_caracter(x, i) then
                aux.sibling := new node;
                aux.sibling.all := (meu_caracter(x, i), null, null);
                aux := aux.sibling;
                posa0(aux, i, x);
                posat := true;
            else
                if aux.child = null then
                    posa0(aux, i, x);
                    posat := true; --TODO
                else
                    aux := aux.child;
                end if;
            end if;

            exit when posat;
        end loop;

        while aux.sibling /= null and aux.value /= '$'  loop
            aux := aux.sibling;
        end loop;
--        if aux.value /= '$' then
--            aux.sibling := new node;
--            aux.sibling.all := ('$', null, null);
--        end if;

    end posa;


    function existeix (s: in diccionari; x: in tparaula) return boolean is
        aux: pnode := s.root;
    begin
        if aux = null then return false; end if;
        for i in 1 .. llargaria(x) loop
            if meu_caracter(x, i) = aux.value then
                if aux.child = null then
                    return false;
                else
                    aux := aux.child;
                end if;
            else
                while aux.sibling /= null and aux.value /= meu_caracter(x, i) loop
                    aux := aux.sibling;
                end loop;
                if aux.sibling = null and aux.value /= meu_caracter(x, i) then
                    return false;
                else
                    if aux.child = null then return false; end if;
                    aux := aux.child; -- TODO arreglar??
                end if;
            end if;
        end loop;

        while aux.sibling /= null and aux.value /= '$'  loop
            aux := aux.sibling;
        end loop;
        if aux.value /= '$' then
            return false;
        end if;
        return true;

    end existeix;

end diccionari_trc;
