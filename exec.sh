#!/bin/bash
# check encoding and convert files

if [ $# != 3 -o "$1" = "--help" ]; then
    echo 'Usage: $0 DICCIONARI TEXT_A_COMPARAR TEXT_DE_SORTIDA' >&2
    exit 1
fi

#Defineing vars
dict=$1
text=$2
output=$3

for f in *.{adb,ads,txt}
do
  encoding=`file -i $f | cut -f 2 -d";" | cut -f 2 -d=`
  case $encoding in
    utf-8)
    iconv -t iso8859-1 -f utf-8 $f > $f.latin1
    mv $f.latin1 $f
    echo "$f: $encoding -> iso8859-1"
    ;;
  esac
done

make

#read -p "Execució amb Cursors [ENTER]" enter

rm $3.tr $3.trc $3.hs $3.cr
touch $3.tr $3.trc $3.hs $3.cr

#echo

./main_cr  $1 $2 $3.cr

echo Cursors fets

#read -p "Execució amb Tries [ENTER]" a
#echo

./main_tr  $1 $2 $3.tr

echo Tries fets


#read -p "Execució amb Tries Compactes [ENTER]" b
#echo

./main_trc  $1 $2 $3.trc

echo Tries compactes fets


#read -p "Execució amb Hashing Obert [ENTER]" c
#echo

./main_hs  $1 $2 $3.hs

echo Hashing fet
