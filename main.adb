with Ada.text_io; use Ada.text_io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with pparaula; use pparaula;
with diccionari_trc; use diccionari_trc;
with Ada.Command_Line; use Ada.Command_Line;
with Ada.Calendar; use Ada.Calendar;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

procedure Main is
    p, p_text: tparaula;
    o: OrigenParaules;

    linea: integer := 0;
    columna: integer := 0;
    a,b: integer;
x: integer := 0;
    ofitxer, ofitxer_text: OrigenParaules;
    paux: tparaula;
    dic_simple: diccionari;
    time_inicial, time_final: Time;

    f_diccionari: Ada.Strings.Unbounded.Unbounded_String;
    f_text: Ada.Strings.Unbounded.Unbounded_String;
    f_sortida: Ada.Strings.Unbounded.Unbounded_String;

    duracio: duration;
    minuts, segons, milesimes: duration;
    package Duration_IO is new Fixed_IO(Duration);

begin
--->de aki


    if argument_count /= 3 then put("Error de parÓmetres -- US: ./nom_programa nom_diccionari nom_text nom_sortida"); new_line;
    else
	f_diccionari := Ada.Strings.Unbounded.to_unbounded_string(argument(1));
	f_text := Ada.Strings.Unbounded.to_unbounded_string(argument(2));
	f_sortida := Ada.Strings.Unbounded.to_unbounded_string(argument(3));
    end if;

    time_inicial := clock;
    open(o, Ada.Strings.Unbounded.to_string(f_text));
    buit(dic_simple);
    --open(ofitxer, "simple.dic.txt");
    open(ofitxer, Ada.Strings.Unbounded.to_string(f_diccionari));
    while not eof(ofitxer) loop
        get(ofitxer, paux, a, b);
        posa(dic_simple, paux);
        new_line;
    end loop;


    put_line("DONE!... Introdueix paraules:");
    --open(ofitxer_text, "text.txt");
    while not eof(o) loop
        x := x + 1;
        get(o, p, linea, columna);

        if not existeix(dic_simple, p)
           then
	           put(p); new_line;
                put("Linea: "); put(linea); new_line;
                put("Columna: "); put(columna); new_line;new_line;
        end if;
    end loop;
    put("--->"); put(x); new_line;
    time_final := clock;
    duracio :=  time_final - time_inicial;
    minuts := duration(Integer(duracio/60+0.5)-1);
    segons := duration(Integer(duracio-minuts*60+0.5)-1);
    milesimes := (duracio-minuts*60-segons)*1000;
    put("Minuts: ");
    Duration_IO.Put(minuts); new_line;
    put("Segons: ");
    Duration_IO.Put(segons); new_line;
    put("Milesimes: ");
    Duration_IO.Put(milesimes); new_line;


---> asta ki
end Main;
