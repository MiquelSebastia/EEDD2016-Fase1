with Ada.text_io; use Ada.text_io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with pparaula; use pparaula;
with diccionari_tr; use diccionari_tr;
with Ada.Command_Line; use Ada.Command_Line;
with Ada.Calendar; use Ada.Calendar;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

procedure Main_tr is
    p: tparaula;
    o: OrigenParaules;

    linea: integer := 0;
    columna: integer := 0;
    a,b: integer;
    ofitxer: OrigenParaules;
    paux: tparaula;
    dic_simple: diccionari;
    time_inicial, time_final: Time;

    f_diccionari: Ada.Strings.Unbounded.Unbounded_String;
    f_text: Ada.Strings.Unbounded.Unbounded_String;
    f_sortida: Ada.Strings.Unbounded.Unbounded_String;
    fitxer_sortida : file_type;

    duracio: duration;
    minuts, segons, milesimes: duration;
    package Duration_IO is new Fixed_IO(Duration);

begin
--->de aki


    if argument_count /= 3 then put("Error de parÓmetres -- US: ./nom_programa nom_diccionari nom_text nom_sortida"); new_line;
    else
	f_diccionari := Ada.Strings.Unbounded.to_unbounded_string(argument(1));
	f_text := Ada.Strings.Unbounded.to_unbounded_string(argument(2));
	f_sortida := Ada.Strings.Unbounded.to_unbounded_string(argument(3));
    end if;
    open(fitxer_sortida, out_file, Ada.Strings.Unbounded.to_string(f_sortida));

    open(o, Ada.Strings.Unbounded.to_string(f_text));
    buit(dic_simple);
    --open(ofitxer, "simple.dic.txt");
    open(ofitxer, Ada.Strings.Unbounded.to_string(f_diccionari));
    while not eof(ofitxer) loop
        get(ofitxer, paux, a, b);
        posa(dic_simple, paux);
    end loop;


    time_inicial := clock;
    while not eof(o) loop
        get(o, p, linea, columna);

        if not existeix(dic_simple, p)
           then
	        put(fitxer_sortida, "ERROR A: "); put(fitxer_sortida, p); new_line(fitxer_sortida);
                put(fitxer_sortida, "Linea: "); put(fitxer_sortida, linea); new_line(fitxer_sortida) ;
                put(fitxer_sortida, "Columna: "); put(fitxer_sortida, columna); new_line(fitxer_sortida) ;new_line(fitxer_sortida) ;
        end if;
    end loop;
    time_final := clock;
    duracio :=  time_final - time_inicial;
    minuts := duration(Integer(duracio/60+0.5)-1);
    segons := duration(Integer(duracio-minuts*60+0.5)-1);
    milesimes := (duracio-minuts*60-segons)*1000;
    put(fitxer_sortida, "Minuts: ");
    Duration_IO.Put(fitxer_sortida, minuts); new_line(fitxer_sortida) ;
    put(fitxer_sortida, "Segons: ");
    Duration_IO.Put(fitxer_sortida, segons); new_line(fitxer_sortida) ;
    put(fitxer_sortida, "Milesimes: ");
    Duration_IO.Put(fitxer_sortida, milesimes); new_line(fitxer_sortida) ;

end Main_tr;
