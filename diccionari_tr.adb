package body diccionari_tr is

    procedure buit (s: out diccionari) is
    begin
        s.root := new node;
        s.root.all := (others => null);
    end buit;


    procedure posa (s: in out diccionari; x: in tparaula) is
        aux: pnode := s.root;
    begin
        for i in 1 .. llargaria(x)+1 loop
            if aux(meu_caracter(x, i)) /= null then
                aux := aux(meu_caracter(x, i));
            else
                aux(meu_caracter(x, i)) := new node;
                aux(meu_caracter(x, i)).all := (others => null);
                aux:= aux(meu_caracter(x, i));
            end if;
        end loop;
    end posa;


    function existeix (s: in diccionari; x: in tparaula) return boolean is
        aux: pnode := s.root;
    begin
        for i in 1 .. llargaria(x)+1 loop
            if aux(meu_caracter(x, i)) /= null then
                aux := aux(meu_caracter(x, i));
            else
                return false;
            end if;
        end loop;
        return true;
    end existeix;

end diccionari_tr;
