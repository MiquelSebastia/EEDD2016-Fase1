package body diccionari_hs is
    procedure buit (s: out diccionari) is
        td:  dispersion_table renames s.td;
        tid: id_table         renames s.tid;
        nid: name_id          renames s.nid;
        nc:  natural          renames s.nc;
    begin
        for i in hash_index loop td(i):= null_id; end loop;
        nid:= 0; nc:= 0;
        tid(null_id) := (null_id, nc);

    end buit;

    function equal
        (name: in string; s: in diccionari; p: in name_id) return boolean is
        tid: id_table        renames s.tid;
        tc: characters_table renames s.tc;
        nid: name_id         renames s.nid;
        pi, pf: natural;
        i, j:   natural;
    begin
        pi:=tid(p-1).ptc+1;pf:=tid(p).ptc;
        i:= name'first; j:= pi;
        while name(i)=tc(j) and i<name'last and j<pf loop
            i:= i+1; j:= j+1;
        end loop;
        return name(i)=tc(j) and i=name'last and j=pf;
    end equal;

    procedure save_name
        (name: in string; tc: in out characters_table; nc: in out natural) is
    begin
        for i in name'range loop
            nc:= nc+1; tc(nc):=name(i);
        end loop;
    end save_name;

    procedure posa (s: in out diccionari; x: in tparaula) is
        td: dispersion_table  renames s.td;
        tid: id_table renames s.tid;
        tc: characters_table renames s.tc;
        nid: name_id renames s.nid;
        nc: natural renames s.nc;
        name: string := toString(x);

        i: Hash_Type;
        p: name_id;
    begin
        i:= Ada.Strings.hash(name) mod b; p:= td(i);
        while p/=null_id and then not equal(name, s, p) loop
            p:= tid(p).psh;
        end loop;
        if p=null_id then
            --if nid=name_id(max_par)   then raise space_overflow; end if;
            if nc+name'length>max_ch then raise space_overflow; end if;
            save_name(name, tc, nc);
            nid:= nid+1; tid(nid):= (td(i), nc);
            td(i):= nid; p := nid;
        end if;
    end posa;

    function existeix (s: in diccionari; x: in tparaula) return boolean is
        td: dispersion_table  renames s.td;
        tid: id_table renames s.tid;
        tc: characters_table renames s.tc;
        nid: name_id renames s.nid;
        name: string := toString(x);

        i: Hash_Type;
        p: name_id;
    begin
        i:= Ada.Strings.hash(name) mod b; p:= td(i);
        while p/=null_id and then not equal(name, s, p) loop
            p:= tid(p).psh;
        end loop;
        if p=null_id then
            return false;
        end if;
        return true;
    end existeix;

end diccionari_hs;
